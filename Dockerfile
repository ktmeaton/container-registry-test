FROM mambaorg/micromamba:1.4.9

RUN micromamba create  --yes  -c bioconda -n csvtk csvtk
ENV PATH /opt/conda/envs/csvtk/bin:$PATH
